;;; status-line.el --- Status line for Emacs -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Free Software Foundation, Inc.

;; Author: Dídac Cerver <didaccerver@gmail.com>
;; Maintainer: Dídac Cerver <didaccerver@gmail.com>
;; Version: 0.1
;; Package-Requires: ((emacs "27.1"))
;; URL: https://gitlab.com/DidacGit/status-line

;; This file is part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Implement a status line using the built-in `tab-bar-mode'.  Enable
;; the status line by toggling on `status-line-mode' Set
;; `status-line-command' to the shell command that should return the
;; status line string.  Additionally, the default 1 second redisplay
;; time can be changed through `status-line-redisplay-interval'.  An
;; example of a shell script to act as the `status-line-command' can
;; be found in this repository.

;;; Code:

(defgroup status-line nil
  "System status info in the tab bar."
  :group 'convenience)

(defcustom status-line-command "date"
  "Shell command that is called periodically to update the status line.
The command is called asynchronously.  Upon its execution, it
returns the actual string to be displayed as the status line.
For the invocation, it uses `shell-file-name' and
`shell-command-switch'."
  :type '(string))

(defcustom status-line-redisplay-interval 1
  "Describes the periodicity of the redisplay of the status-line.
Defaults to 1 second.  All the different formats of TIME
described in the doc string of `run-at-time' can be used."
  :type '(sexp))

(defvar status-line-string nil
  "String displayed as the status-line.
This is produced by the output of `status-line-command'.")

(defvar status-line-old-tab-bar-p nil
  "Old value of `tab-bar-mode'.
Record this value so it can be reset when the minor mode is disabled.")

(defvar status-line-old-tab-bar-mode-p nil
  "Old value of `tab-bar-mode'.
Record this value so it can be reset when the minor mode is disabled.")

(defvar status-line-old-tab-bar-format nil
  "Old value of `tab-bar-format'.
Record this value so it can be reset when the minor mode is disabled.")

(defun status-line-get-string ()
  "Function called every time that the status line is redisplayed.
Simply returns the value of `status-line-string'."
  status-line-string)

;;;###autoload
(define-minor-mode status-line-mode
  "Toggle the status line in the tab bar."
  :global t
  (if status-line-mode
      ;; Record the old values to reset them when the mode is disabled.
      (progn (setq status-line-old-tab-bar-mode-p tab-bar-mode)
	     (setq status-line-old-tab-bar-format tab-bar-format)
	     (tab-bar-mode)
	     (add-to-list 'tab-bar-format 'tab-bar-format-align-right t)
	     (add-to-list 'tab-bar-format 'status-line-get-string t)
	     (status-line-start-process)
	     (message "Status-Line mode enabled"))
    (progn (setq status-line-mode nil)
	   (setq status-line-string nil)
	   (if status-line-old-tab-bar-mode-p
	       (tab-bar-mode)
	     (tab-bar-mode -1))
	   (when status-line-old-tab-bar-format
	     (setq tab-bar-format status-line-old-tab-bar-format))
	   (message "Status-Line mode disabled"))))

(defun status-line-start-process ()
  "Start the shell process which runs recursively."
  (let ((output-buffer (generate-new-buffer " *status-line-temp*")))
    (set-process-sentinel
     (start-process "status-line-shell-command"
		    output-buffer
		    shell-file-name
		    shell-command-switch
		    status-line-command)
     (lambda (process signal)
       (when (and status-line-mode (memq (process-status process) '(exit signal)))
         (with-current-buffer output-buffer
           (setq status-line-string
		 (buffer-substring-no-properties
		  (point-min)
		  (point-max))))
	 (kill-buffer output-buffer)
	 (force-mode-line-update) 	; Redisplays also the tab-bar.
	 (run-at-time status-line-redisplay-interval
		      nil
		      'status-line-start-process))))))

(provide 'status-line)
;;; status-line.el ends here
