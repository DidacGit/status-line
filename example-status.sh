#!/bin/sh
# Display an example system status line.

# Get the used memory.
used_mem() (
    mem_tot=$(free | awk 'NR==2 {print $2}')
    mem_used=$(free | awk 'NR==2 {print $3}')
    echo "scale = 2; ($mem_used / $mem_tot) * 100" | bc | sed 's/\..*$/%/'
)

# Get the used CPU.
used_cpu() (
    cpu_idle=$(vmstat | awk 'NR==3 {print $15}')
    echo "$((100 - cpu_idle))"%
)

# Get the battery status.
bat_stat() (
    if $(cat /sys/class/power_supply/BAT0/status | grep -iq discharging); then
	echo '(-)'
    else
	echo '(+)'
    fi
)

# Get the volume status.
vol_stat() (
    if [ $(pulsemixer --get-mute) -eq 1 ]; then
	echo -
    else
	pulsemixer --get-volume | cut -d ' ' -f 1 | sed 's/.*/&%/'
    fi
)

echo "CPU: $(used_cpu) | MEM: $(used_mem) | BAT: $(cat /sys/class/power_supply/BAT0/capacity)% $(bat_stat) | VOL: $(vol_stat) | $(date '+%H:%M %d-%m-%Y') "
